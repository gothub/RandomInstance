from typing import Union

from fastapi import FastAPI
from fastapi.responses import RedirectResponse
import requests
import random

app = FastAPI()

@app.get("/")
def read_root():
    return {"Response": "Hello World"}

@app.get("/random")
def redirect():
    # get GotHub instances json from codeberg
    r = requests.get("https://codeberg.org/gothub/gothub-instances/raw/branch/master/instances.json")

    # get random instance
    instance = random.choice(r.json())

    # redirect to random instance
    return RedirectResponse(url=instance["link"])

@app.get("/random/dev")
def redirect_dev():
    # get GotHub instances json from codeberg
    r = requests.get("https://codeberg.org/gothub/gothub-instances/raw/branch/master/instances.json")

    # get random instance, branch = dev
    instance = random.choice([i for i in r.json() if i["branch"] == "dev"])

    # redirect to random instance
    return RedirectResponse(url=instance["link"])

@app.get("/random/master")
def redirect_master():
    # get GotHub instances json from codeberg
    r = requests.get("https://codeberg.org/gothub/gothub-instances/raw/branch/master/instances.json")

    # get random instance, branch = master
    instance = random.choice([i for i in r.json() if i["branch" == "master"]])

    # redirect to random instance
    return RedirectResponse(url=instance["link"])
