FROM python:3.9-alpine3.17

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY . /code

CMD ["uvicorn", "main:app", "--log-level", "critical", "--host", "0.0.0.0", "--port", "8080"]
